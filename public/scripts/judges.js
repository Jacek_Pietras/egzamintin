/*jshint globals: true, browser: true, jquery: true, -W117*/

$(document).ready(function () {
    var competitionField = $("#competitionField");
    var nameField = $("#nameField");
    var surnameField = $("#surnameField");
    var loginForm = $("#login");
    var keyField = $("#keyField");
    var $n = $("#horseNumber");
    var $n2 = $("#horseNumber2");
    var socket;
    var clientSocket;
    var i;
    var counter = 5;
    var valOne;
    var valTwo;
    var valThree;
    var valFour;
    var valFive;

    //    $("#competitionAdd").click(function () {
    //
    //        $n.val(Number($n.val()) + 1);
    //
    //    });

    if (!socket || socket.connected) {
        socket = io({
            forceNew: true
        });
    }

    socket.on('connect', function () {
        clientSocket = socket.id;
        socket.emit("wyslijID", clientSocket);
        socket.emit("consoleKey");
        socket.on("sendWynik", function (odebrane) {
            i = odebrane;
            console.log("Wylosowany numerek sędziego " + i);
            socket.on("sendKey", function (result) {
                console.log("Sekretny klucz: " + JSON.stringify(result[odebrane].key));
            });
            // socket.on("sendFindKey", function (data) {
            //   console.log("przepraszam, wyswietlam: " +data);
            // });
        });
        // socket.on("key", function (unlockKey) {
        //   console.log(unlockKey);
        // });
    });

    $("#verify").click(function (event) {
        socket.emit("start");
        socket.on("sendResult", function (judges) {
            //console.log("Sekretny klucz: " + judges[i].key);
            // console.log(i);
            // console.log("patrz" + judges[i].key);
            // console.log(keyField.val());
            //if (judges[i].name === nameField.val() && judges[i].surname === surnameField.val()) {
            if (judges[i].key == keyField.val()) {
                //
                // console.log(i);
                // console.log(judges[i].name);
                // console.log(judges[i].surname);
                competitionField.show();
            } else {
                alert("Niepoprawne dane");
                competitionField.hide();
            }
        });
    });

    $("#competitionAdd").click(function (event) {
        valOne =  $("#competitionA").val();
        valTwo =  $("#competitionB").val();
        valThree =  $("#competitionC").val();
        valFour =  $("#competitionD").val();
        valFive =  $("#competitionE").val();
        event.preventDefault();
        socket.emit("all");
        socket.emit("data", {
            //name   ::   id
            startNumber: $("#horseNumber").val(),
            showA: $("#competitionA").val(),
            showB: $("#competitionB").val(),
            showC: $("#competitionC").val(),
            showD: $("#competitionD").val(),
            showE: $("#competitionE").val()
        });

        while (counter >= 0) {
            counter--;
            alert("Formularz wysłany poprawnie. Pozostało koni do oceny: " + counter);
            console.log("Formularz wysłany poprawnie. Pozostało koni do oceny: " + counter);
            break;
        }

        if (counter === 0) {
            alert("Wszystkie konie ocenione poprawnie. Dziękujemy za udział w głosowaniu");
            loginForm.hide();
            competitionField.hide();
        }


        $("#competitionAdd").prop('disabled', true);
        $("#competitionField").hide();
        $("#editField").show();

        //ustawienie wartosci do formularza edytujacego, tak aby pokazac wartosci jakie byly wczesniej podczas glosowania
        $("#compA").val(valOne);
        $("#compB").val(valTwo);
        $("#compC").val(valThree);
        $("#compD").val(valFour);
        $("#compE").val(valFive);

        $("#competitionEdit").click(function () {
          socket.emit("changedData", {
              //startNumber: $("#horseNumber").val(),
              showA2: $("#compA").val(),
              showB2: $("#compB").val(),
              showC2: $("#compC").val(),
              showD2: $("#compD").val(),
              showE2: $("#compE").val()
          });

        });

        socket.on("nextForm", function(allVoted) {
          alert(allVoted);
          if (allVoted === 5) {
            $n2.val(Number($n2.val()) + 1);
            $("#editField").hide();
            $("#competitionAdd").prop('disabled', false);
            $("#competitionField").show();
            $("#editField").hide();
          }
        });
          $n.val(Number($n.val()) + 1);
    });

    socket.on("callback", function (data) {
        console.log(data.done);
    });
});
