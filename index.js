/*jshint globals: true, node: true*/

require("./db");

var express = require("express");
var mongoose = require("mongoose");
var morgan = require("morgan");
var _ = require("underscore");
var async = require("async");
var lessMiddleware = require("less-middleware");
var path = __dirname + "/public/views/";

var app = express();
var httpServer = require("http").Server(app);
var io = require("socket.io")(httpServer);

var port = process.env.PORT || 8000;

app.use(morgan("dev"));

app.use(lessMiddleware(__dirname + "/public", {
	debug: true,
	compress: true
}));

app.use(express.static(__dirname + '/public'));

app.use("/jquery", express.static("bower_components/jquery/dist/"));

app.get("/", function(req, res) {
	res.sendFile(path + "menu.html");
});

app.get("/sedziowie", function(req, res) {
	res.sendFile(path + "judges.html");
});

app.get("/widzowie", function(req, res) {
	res.sendFile(path + "audience.html");
});

var Horses = mongoose.model("horses");
var Judges = mongoose.model("judges");
var ChatSchema = mongoose.model("Message");

function arrayShuffle() {
	var i = this.length,
		j, temp;
	if (i === 0) return false;
	while (--i) {
		j = Math.floor(Math.random() * (i + 1));
		temp = this[i];
		this[i] = this[j];
		this[j] = temp;
	}
}

Array.prototype.shuffle = arrayShuffle;

var start = 1;
var end = 9;
var numbers = [];
for (var i = start; i <= end; i++) {
	numbers.push(i);
}

numbers.shuffle();
console.log(numbers);

var users = [];
var selected = [];
var login = {};
var keys = [];
var persons = [];
var votes = 0;
//var counter;
io.sockets.on("connection", function(socket) {

	users.push(socket.id);

	console.log("Przyszedł " + users);

	var findKey = Judges.find({}, function(err, result) {
		if (err) {
			return console.log(err);
		} else {
			socket.on("consoleKey", function() {
				socket.emit("sendKey", result);
			});
		}
	});

	socket.on("wyslijID", function(odbierz) {
		persons.push(odbierz);
    //counter = persons;
		console.log("Przyszedł użytkownik o id " + persons);
		for (var j = 0; j < users.length; j++) {
			if (users[j] === "/#" + odbierz) {
				if (persons.length <= 5) {
					keys.push(numbers.pop());
					io.sockets.connected[users[j]].emit("sendWynik", keys[0]);
					keys.splice(0, keys[0]);
				} else {
					console.log("Liczba możliwych sędziow przekroczona");
				}
			}
		}
	});

  var increase = function () {
    votes++;
  };

  socket.on("all", function () {
    increase();
    console.log("musi byc piec: " + votes);
    //io.sockets.connected["/#"+counter[0]].emit("nextForm", votes);
    io.emit("nextForm", votes);
    if (votes ===5) {
      votes = 0;
    }
  });

	socket.on("data", function(data) {
		new Horses({
			_id: mongoose.Types.ObjectId(),
			startNumber: data.startNumber,
			competitionA: data.showA,
			competitionB: data.showB,
			competitionC: data.showC,
			competitionD: data.showD,
			competitionE: data.showE
		}).save(function(err, doc) {
			if (err) {
				return console.log(err);
			} else {
				socket.emit("callback", {
					done: "Done",
					data: data
				});
			}
		});

		socket.on("changedData", function (editedData) {
			Horses.find({
				startNumber: data.startNumber,
				competitionA: data.showA,
				competitionB: data.showB,
				competitionC: data.showC,
				competitionD: data.showD,
				competitionE: data.showE
				 }, function(err, result) {
		    if(err) {
		      console.log(err);
		    } else {
		      console.log("szukany kon:  " + result);
		    }
		  }).update({
				competitionA: editedData.showA2,
				competitionB: editedData.showB2,
				competitionC: editedData.showC2,
				competitionD: editedData.showD2,
				competitionE: editedData.showE2
		  }).exec();
		});

	});

	var findJudge = Judges.find({}, function(err, judges) {
		if (err) {
			return console.log(err);
		} else {
			console.log("Sędziowie:\n" + judges);
			socket.on("start", function() {
				//socket.emit("judgeNumber", num);
				socket.emit("sendResult", judges);
			});
		}
	});

	setInterval(function () {
        var showScore = Horses.find({}, function (err, horses) {
            if (err) {
                return console.log(err);
            } else {
                var wyniki = {};
                var wyniki1 = {};
                var wyniki2 = {};
                var wyniki3 = {};
                var wyniki4 = {};
                var wyniki5 = {};

                var kategoriaA1 = 0;
                var kategoriaB1 = 0;
                var kategoriaC1 = 0;
                var kategoriaD1 = 0;
                var kategoriaE1 = 0;

                var kategoriaA2 = 0;
                var kategoriaB2 = 0;
                var kategoriaC2 = 0;
                var kategoriaD2 = 0;
                var kategoriaE2 = 0;

                var kategoriaA3 = 0;
                var kategoriaB3 = 0;
                var kategoriaC3 = 0;
                var kategoriaD3 = 0;
                var kategoriaE3 = 0;

                var kategoriaA4 = 0;
                var kategoriaB4 = 0;
                var kategoriaC4 = 0;
                var kategoriaD4 = 0;
                var kategoriaE4 = 0;

                var kategoriaA5 = 0;
                var kategoriaB5 = 0;
                var kategoriaC5 = 0;
                var kategoriaD5 = 0;
                var kategoriaE5 = 0;

                var iloscKoni1 = 0;
                var iloscKoni2 = 0;
                var iloscKoni3 = 0;
                var iloscKoni4 = 0;
                var iloscKoni5 = 0;


                var array = [];
                for (var i = 0; i < horses.length; i++) {

                    if (horses[i].startNumber === 1) {
                        iloscKoni1++;
                        kategoriaA1 += horses[i].competitionA;
                        kategoriaB1 += horses[i].competitionB;
                        kategoriaC1 += horses[i].competitionC;
                        kategoriaD1 += horses[i].competitionD;
                        kategoriaE1 += horses[i].competitionE;

                        wyniki1 = {
                            numerStartowy: 1,
                            srednia: (kategoriaA1 + kategoriaB1 + kategoriaC1 + kategoriaD1 + kategoriaE1) / (iloscKoni1 * 5)

                        };

                    }


                    if (horses[i].startNumber === 2) {
                        iloscKoni2++;
                        kategoriaA2 += horses[i].competitionA;
                        kategoriaB2 += horses[i].competitionB;
                        kategoriaC2 += horses[i].competitionC;
                        kategoriaD2 += horses[i].competitionD;
                        kategoriaE2 += horses[i].competitionE;

                        wyniki2 = {
                            numerStartowy: 2,
                            srednia: (kategoriaA2 + kategoriaB2 + kategoriaC2 + kategoriaD2 + kategoriaE2) / (iloscKoni2 * 5)

                        };

                    }


                    if (horses[i].startNumber === 3) {
                        iloscKoni3++;
                        kategoriaA3 += horses[i].competitionA;
                        kategoriaB3 += horses[i].competitionB;
                        kategoriaC3 += horses[i].competitionC;
                        kategoriaD3 += horses[i].competitionD;
                        kategoriaE3 += horses[i].competitionE;

                        wyniki3 = {
                            numerStartowy: 3,
                            srednia: (kategoriaA3 + kategoriaB3 + kategoriaC3 + kategoriaD3 + kategoriaE3) / (iloscKoni3 * 5)

                        };

                    }



                    if (horses[i].startNumber === 4) {
                        iloscKoni4++;
                        kategoriaA4 += horses[i].competitionA;
                        kategoriaB4 += horses[i].competitionB;
                        kategoriaC4 += horses[i].competitionC;
                        kategoriaD4 += horses[i].competitionD;
                        kategoriaE4 += horses[i].competitionE;

                        wyniki4 = {
                            numerStartowy: 4,
                            srednia: (kategoriaA4 + kategoriaB4 + kategoriaC4 + kategoriaD4 + kategoriaE4) / (iloscKoni4 * 5)

                        };

                    }


                    if (horses[i].startNumber === 5) {
                        iloscKoni5++;
                        kategoriaA5 += horses[i].competitionA;
                        kategoriaB5 += horses[i].competitionB;
                        kategoriaC5 += horses[i].competitionC;
                        kategoriaD5 += horses[i].competitionD;
                        kategoriaE5 += horses[i].competitionE;

                        wyniki5 = {
                            numerStartowy: 5,
                            srednia: (kategoriaA5 + kategoriaB5 + kategoriaC5 + kategoriaD5 + kategoriaE5) / (iloscKoni5 * 5)

                        };

                    }

                }

                if (!isNaN(wyniki1.numerStartowy) && !isNaN(wyniki1.srednia)) {
                    array.push(wyniki1);
                }

                if (!isNaN(wyniki2.numerStartowy) && !isNaN(wyniki2.srednia)) {
                    array.push(wyniki2);
                }

                if (!isNaN(wyniki3.numerStartowy) && !isNaN(wyniki3.srednia)) {
                    array.push(wyniki3);
                }
                if (!isNaN(wyniki4.numerStartowy) && !isNaN(wyniki4.srednia)) {
                    array.push(wyniki4);
                }
                if (!isNaN(wyniki5.numerStartowy) && !isNaN(wyniki5.srednia)) {
                    array.push(wyniki5);
                }

                var sortowanie = _.sortBy(array, 'srednia');
                //                console.log("Wartość array " + JSON.stringify(array));
                socket.emit("sendScore", array);
            }
        });
    }, 1000);




	ChatSchema.find({}, function(err, docs) {

		if (err) {

			console.log(err);

		} else {

			socket.emit("load old msg", docs);

		}

	});


	socket.on("new user", function(data, callback) {

		if (data in login) {

			callback(false);

		} else {

			callback(true);

			socket.nickname = data;

			login[socket.nickname] = socket;

			updateNicknames();

		}

	});


	socket.on("send message", function(data, callback) {

		var msg = data.trim();

		console.log("Wiadomość po przetworzeniu: " + msg);

		if (msg.substr(0, 3) === "/w ") {

			msg = msg.substr(3);

			var ind = msg.indexOf(' ');

			if (ind !== -1) {

				var name = msg.substring(0, ind);

				var messg = msg.substring(ind + 1);

				if (name in login) {

					login[name].emit("whisper", {

						msg: messg,

						nick: socket.nickname

					});

					console.log("Wysłana wiadomość " + msg);

				} else {

					callback("Błąd! Podaj właściwą nazwe użytkownika ");

				}

			} else {

				callback("Błąd! wprowadź ponownie wiadomość do użytkownika");

			}

		} else {

			var newMsg = new ChatSchema({

				msg: msg,

				nick: socket.nickname

			});

			newMsg.save(function(err) {

				if (err) {

					console.log(err);

				}

			});

			io.sockets.emit("new message", {

				msg: msg,

				nick: socket.nickname

			});

		}

	});


	function updateNicknames() {

		io.sockets.emit("usernames", Object.keys(login));

	}


	socket.on('disconnect', function() {

		var index = users.indexOf(socket.id);

		if (index != -1) {

			users.splice(index, 1);

		}


		var index2 = persons.indexOf(socket.id);

		if (index != -1) {

			persons.splice(index2, 1);

		}


		console.log("Osoba o (id= " + socket.id + ") zakończyła ocenianie");


		if (!socket.nickname) return;

		delete login[socket.nickname];

		updateNicknames();

	});

});


httpServer.listen(port, function() {

	console.log("Serwer HTTP działa na porcie " + port);

});
